package main

import "testing"

func TestFoo(t *testing.T) {
	expected := "baz baz"
	if v := foo("baz"); v != expected {
		t.Errorf("Expected return value: %s, got: %s", expected, v)
	}
}

func TestBar(t *testing.T) {
	expected := "https://example.com"
	if v := bar("Foo Bar https://example.com Baz"); v != expected {
		t.Errorf("Expected return value: %s, got: %s", expected, v)
	}
}
