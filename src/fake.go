package main

import (
	"fmt"

	"github.com/sbstjn/link"
)

func bar(baz string) string {
	return link.Get(baz).String()
}

func foo(bar string) string {
	return fmt.Sprintf("%s %s", bar, bar)
}
