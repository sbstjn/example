lint:
	@ golint ./...

test: export GO111MODULE=on
test:
	@ go test ./...

build: export GO111MODULE=on
build:
	@ go build -o dist/example ./src

run: export GO111MODULE=on
run:
	@ go run ./src

package:
	@ echo "Done!"

.PHONY: build lint package run test 